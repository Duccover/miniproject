//
//  HomeViewController2.swift
//  miniProject
//
//  Created by Duc on 6/21/21.
//

import UIKit

class HomeViewController2: UIViewController {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btn: UIButton!
    {
        didSet{
            btn.layer.cornerRadius = 15
        }
    }

    var room1 = ""
    var room2 = ""
    
    var image = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        img.image = image
        
    }
    
    @IBAction func btnDatphong(_ sender: Any) {
        
        
        let vc:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = vc.instantiateViewController(withIdentifier: "bookphong") as! Bookphong
        vc1.roomPhong = room1
        vc1.roomSoluong = room2
        vc1.imga = image
        self.navigationController?.pushViewController(vc1, animated: true)
        
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
