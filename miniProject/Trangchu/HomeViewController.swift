//
//  HomeViewController.swift
//  miniProject
//
//  Created by Bui Ngoc Son on 6/15/21.
//

import UIKit
import DropDown

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let dropDown = DropDown()
    let value = ["Tất Cả","CharmVit","Nguyễn Quốc Trị"]
    var rooms = ["Phòng hội nhập" , "Phòng sáng tạo","Phòng tốc độ"]
    var maxs = ["Max: 18 người" , "Max: 10 người","Max: 4 người"]
    var imgs = [ "Rectangle 3124","Rectangle 3125", "Rectangle 3126"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        navigationController?.navigationBar.isHidden = true
        lblText.text = "Tất Cả"
        dropDown.anchorView = viewLabel
        dropDown.dataSource = value
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblText.text = value[index]
   
    }
    }
    
    @IBAction func btnAction(_ sender: Any) {
        dropDown.show()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhongTableViewCell", for: indexPath) as! PhongTableViewCell
        let room = rooms[indexPath.row]
        let max = maxs[indexPath.row]
        let img = imgs[indexPath.row]
        cell.lblRoom.text = room
        cell.lblMax.text = max
        cell.imgView.image = UIImage(named: img)
        cell.lblview.text = "8"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc1 = storyboard?.instantiateViewController(withIdentifier: "homeview2") as! HomeViewController2
        vc1.image = UIImage(named: imgs[indexPath.row])!
        vc1.room1 = rooms[indexPath.row]
        vc1.room2 = maxs[indexPath.row]
        self.navigationController?.pushViewController(vc1, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 236
    }
}
