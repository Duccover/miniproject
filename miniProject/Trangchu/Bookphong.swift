//
//  Bookphong.swift
//  miniProject
//
//  Created by Duc on 6/9/21.
//

import UIKit

class CustomObject {
    var name: String
    var email: String
    var imgs: UIImage
    var isSelected: Bool
    
    init(name: String, email: String, imgs: UIImage) {
        self.name = name
        self.email = email
        self.imgs = imgs
        self.isSelected = false
    }
    func clone() -> CustomObject {
         let newObject = CustomObject(name: self.name, email: self.email , imgs: self.imgs)
         newObject.isSelected = self.isSelected
         return newObject
     }
 }



class Bookphong: UIViewController,PopupDelegate,popupSuccess,popupChon {
    @IBOutlet weak var imgNen: UIImageView!
    @IBOutlet weak var lblPhong: UILabel!
    @IBOutlet weak var lblSoLuong: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var txttieude: UITextField!
    @IBOutlet weak var txtmota: UITextField!
    @IBOutlet weak var btn: UIButton!
    {
        didSet{
            btn.layer.cornerRadius = 15
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!
    
    var listImages: [CustomObject] = [
            CustomObject(name:"Đào Thị Hạnh",email:"Handdt@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh")!),
            CustomObject(name:"Trần Huyền Trang",email:"Trangth@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh2")!),
            CustomObject(name:"Nguyễn Thịnh Cường",email:"Cuongnt@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh3")!),
            CustomObject(name:"Trần Huỳnh Đức",email:"Ducth@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh")!),
            CustomObject(name:"Bùi Ngọc Sơn",email:"Sonbn@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh2")!),
            CustomObject(name:"Nguyễn Thị Hoà",email:"Hoant@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh3")!)
            ]
    
    var roomPhong = ""
    var roomSoluong = ""
    var imga = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPhong.text = roomPhong
        lblSoLuong.text = roomSoluong
        imgNen.image = imga
        
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
 
    
    func closeTap(_ chuoi: String, chuoi2:String) {
        lbl1.text = chuoi
        lbl2.text = chuoi2
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    func closeTap1() {
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    func closetap2() {
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    @IBAction func btntime(_ sender: UIButton) {
        
        var mypop:ChonTG!
        mypop = ChonTG(nibName: "ChonTG", bundle: nil)
        self.view.alpha = 1.0
        mypop.closePopup = self
        self.presentpopupViewController(popupViewController: mypop, animationType: .BottomTop, completion: {()
            -> Void in
        })
    }
    
    @IBAction func btnChon(_ sender: UIButton) {
        var mypop2:Moithanhvien!
        mypop2 = Moithanhvien(nibName:"Moithanhvien" , bundle: nil)
        self.view.alpha = 1.0
        mypop2.closePopup2 = self
        self.presentpopupViewController(popupViewController: mypop2, animationType: .BottomTop, completion: {()
            ->Void in
        })
        
    }
    
    @IBAction func btnbookphong(_ sender: UIButton) {
        var mypop1:DPTCong!
        mypop1 = DPTCong(nibName: "DPTCong", bundle: nil)
        self.view.alpha = 1.0
        mypop1.closePopup1 = self
        self.presentpopupViewController(popupViewController: mypop1, animationType: .BottomTop, completion: {()
            -> Void in
        })
    }
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension Bookphong: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listImages.filter({ $0.isSelected }).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        //sai
        cell.largeContentImage = listImages[indexPath.row].imgs
        
        return cell
    }
}
extension Bookphong: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
}
extension Bookphong: SelectImageViewControllerDelegate {
    func selectImageViewController(didSave vc: Moithanhvien, listImages: [CustomObject]) {
        self.listImages = listImages
        collectionView.reloadData()
        
    }
    
    
}
