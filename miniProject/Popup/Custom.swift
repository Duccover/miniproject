//
//  Custom.swift
//  miniProject
//
//  Created by Duc on 6/14/21.
//

import UIKit

class Custom: UITableViewCell{
    @IBOutlet weak var check: UIButton!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var img: UIImageView!
    var customObject: CustomObject!
    override func awakeFromNib() {
        super.awakeFromNib()
   
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

         
        }
        
        func bindData(customObject: CustomObject) {
            self.customObject = customObject
            check.isSelected = customObject.isSelected
        }
    
    public func doChangeLayout(data: String,data2:String,img:UIImage) {
        self.lblname.text = data
        self.lblEmail.text = data2
        self.img.image = img
        
    }
    public func btnChanlayout(data: String,data2:String,img:UIImage,select:Bool) {
            self.lblname.text = data
            self.lblEmail.text = data2
            self.img.image = img
            self.check.isSelected = select
        }
    
    @IBAction func btnCheck(_ sender: UIButton) {
        customObject.isSelected = !customObject.isSelected
                var pathImage = ""
                
                if customObject.isSelected {
                    pathImage = "check2"
                } else {
                    pathImage = "check"
                }
                sender.setBackgroundImage((UIImage(named: pathImage)), for: UIControl.State.normal)

        
    }
}
