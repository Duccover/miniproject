//
//  Moithanhvien.swift
//  miniProject
//
//  Created by Duc on 6/11/21.
//

import UIKit

protocol popupChon{
    func closetap2()
}
protocol SelectImageViewControllerDelegate: class {
    func selectImageViewController(didSave vc: Moithanhvien, listImages: [CustomObject])
}

class Moithanhvien: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{
    weak var delegate: SelectImageViewControllerDelegate?
        var listImages: [CustomObject] = []
    
    var name = ["Đào Thị Hạnh","Trần Huyền Trang","Nguyễn Thịnh Cường","Trần Huỳnh Đức","Bùi Ngọc Sơn","Nguyễn Thị Hoà"]
    let email = ["Handdt@ttc-solutions.com.vn","Trangth@ttc-solutions.com.vn","Cuongnt@ttc-solutions.com.vn","Ducth@ttc-solutions.com.vn","Sonbn@ttc-solutions.com.vn","Hoant@ttc-solutions.com.vn"]
    
    var imgs = ["gaixinh","gaixinh2","gaixinh3","gaixinh","gaixinh2","gaixinh3"]
    
    var filteredData:[String]!
    
    typealias Item = (name:String, email: String, imgs:UIImage?)
    
    var filtered: [Item] = []
    var searchActive : Bool = false
    var items: [Item] = [
        (name:"Đào Thị Hạnh",email:"Handdt@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh")),
        (name:"Trần Huyền Trang",email:"Trangth@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh2")),
        (name:"Nguyễn Thịnh Cường",email:"Cuongnt@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh3")),
        (name:"Trần Huỳnh Đức",email:"Ducth@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh")),
        (name:"Bùi Ngọc Sơn",email:"Sonbn@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh2")),
        (name:"Nguyễn Thị Hoà",email:"Hoant@ttc-solutions.com.vn",imgs:UIImage(named: "gaixinh3"))
        ]
    
    var activeItems: [Item] {
            if searchActive {
                return filtered
            } else {
                return items
            }
        }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btn:UIButton!{
        didSet{
            btn.layer.cornerRadius = 10
        }
    }
    var closePopup2:popupChon?
    override func viewDidLoad() {
        super.viewDidLoad()
        filteredData = email
        initView()
        
    }
    func binData(listImages:[CustomObject]){
        self.listImages = listImages.map{$0.clone()
        }}
    private func initView() {
        tableView.register(UINib(nibName: "Custom", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filtered.count
        }
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as? Custom else {
            
            return UITableViewCell()
        }
        
        cell.doChangeLayout(data: name[indexPath.row],data2: filteredData[indexPath.row], img:UIImage(named: imgs[indexPath.row])!)
        cell.lblname.text = activeItems[indexPath.row].name
        cell.img.image = activeItems[indexPath.row].imgs
        cell.lblEmail.text = activeItems[indexPath.row].email
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        filtered = items.filter { item in
            item.email.localizedCaseInsensitiveContains(searchText)
        }
      
        searchActive = !filtered.isEmpty
        self.tableView.reloadData()
    }
    @IBAction func btnExit(_ sender: Any) {
        self.closePopup2?.closetap2()
    }
    
    @IBAction func btnDongy(_ sender: Any) {
        
    }
    
}
