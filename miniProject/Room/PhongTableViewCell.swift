//
//  PhongTableViewCell.swift
//  miniProject
//
//  Created by Bui Ngoc Son on 6/15/21.
//

import UIKit

class PhongTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRoom: UILabel!
    @IBOutlet weak var lblMax: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblview: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
