//
//  ViewController.swift
//  miniProject
//
//  Created by Duc on 6/5/21.
//

import UIKit
class Login: UIViewController,popupCheck1,popupCheck2,popupCheck3,popupCheck4 {
    func closecheck1() {
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    func closecheck2() {
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    func closecheck3() {
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    func closecheck4() {
        self.dismissPopupViewController(animationType: SLpopupViewAnimationType.Fade)
    }
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMatkhau: UITextField!
    @IBOutlet weak var btn: UIButton!
    {
        didSet{
            btn.layer.cornerRadius = 10
        }
    }
    
    var iconclick = false
    let imageicon  = UIImageView()

    let reachability = try! Reachability()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        imageicon.image = UIImage(named: "close")
        let contenview = UIView()
        contenview.addSubview(imageicon)
        contenview.frame = CGRect(x: 0, y: 0, width: UIImage(named: "close")!.size.width, height:UIImage(named: "close")!.size.height )
        imageicon.frame = CGRect(x: 170, y:85 , width: 30, height: 30)
        txtMatkhau.rightView = contenview
        txtMatkhau.rightViewMode = .always
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tap:)))
        imageicon.isUserInteractionEnabled = true
        imageicon.addGestureRecognizer(tap)
        
        
    }
    func showAlert(){
        let alert = UIAlertController(title: "Thông báo", message: "Vui lòng kiểm tra kết nối của bạn!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            
            self.reachability.whenReachable = { reachability in
                if reachability.connection == .wifi{
                    print("da ket noi")
                }
                else{
                    print("mat ket noi")
                }
                self.view.window?.rootViewController?.dismiss(animated: true)
             }
            self.reachability.whenUnreachable = { _ in
                print(" no reachable")
                self.showAlert()
            }
            do{
                try  self.reachability.startNotifier()
            }catch{
                print("unable to start")
            }
            
        }
    }
    deinit {
        reachability.stopNotifier()
    }

    
    @objc func imageTapped (tap: UITapGestureRecognizer)
    
    {
        let tapImage = tap.view as! UIImageView
        if iconclick{
            iconclick = false
            tapImage.image = UIImage(named: "open")
            txtMatkhau.isSecureTextEntry = false
        }
        else{
            iconclick = true
            tapImage.image = UIImage(named: "close")
            txtMatkhau.isSecureTextEntry = true
        }
        
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        
        guard let email = txtEmail.text , txtEmail.text?.count != 0
        else {
            
            var mycheck1:checkLogin1!
            mycheck1 = checkLogin1(nibName: "checkLogin1", bundle: nil)
            self.view.alpha = 1.0
            mycheck1.popup1 = self
            self.presentpopupViewController(popupViewController: mycheck1, animationType: .BottomTop, completion: {()
                -> Void in
            })
            return
        }
        //MARK: email hoac matkhau khong dc de trong
        guard let matkhau = txtMatkhau.text , txtMatkhau.text?.count != 0
        else {
            
            var mycheck1:checkLogin1!
            mycheck1 = checkLogin1(nibName: "checkLogin1", bundle: nil)
            self.view.alpha = 1.0
            mycheck1.popup1 = self
            self.presentpopupViewController(popupViewController: mycheck1, animationType: .BottomTop, completion: {()
                -> Void in
            })
            return
        }
        
        
        
        if txtEmail.text == "quynhdd@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "anhbtn@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "phongdx@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "huongnt@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "hant@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "maint@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "hoant@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "thuytt@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "huent@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "hongct@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "hieupa@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "khanhnhq@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "tungbq@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "dund@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "hiepnh@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "sonpn@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "quyetpv@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "sinhpv@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "lampv@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "baopt@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "toanlt@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "datnb@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "sonnt2@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "tungtd1@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "tuannh@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "dupt@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "namnt@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "longtq@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "sontn@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "sangtq@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "namnp2@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "diennv@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "cuongnm@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "ducth@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
            || txtEmail.text == "sonbn@ttc-solutions.com.vn" && txtMatkhau.text == "12345678"
        
        
        {
            //MARK: dang nhap thanh cong
            
            let vc:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc1 = vc.instantiateViewController(withIdentifier: "home") as! HomeViewController
            self.navigationController?.pushViewController(vc1, animated: true)
        }
        else if isValidEmail(emailId: email) == false {
            
            //MARK: email khong dung dinh dang
            
            var mycheck3:checkLogin3!
            mycheck3 = checkLogin3(nibName: "checkLogin3", bundle: nil)
            self.view.alpha = 1.0
            mycheck3.popup3 = self
            self.presentpopupViewController(popupViewController: mycheck3, animationType: .BottomTop, completion: {()
                -> Void in
            })
        }
        else if txtEmail.text != "quynhdd@ttc-solutions.com.vn",
                txtEmail.text != "anhbtn@ttc-solutions.com.vn",
                txtEmail.text != "phongdx@ttc-solutions.com.vn",
                txtEmail.text != "huongnt@ttc-solutions.com.vn",
                txtEmail.text != "hant@ttc-solutions.com.vn",
                txtEmail.text != "maint@ttc-solutions.com.vn",
                txtEmail.text != "hoant@ttc-solutions.com.vn",
                txtEmail.text != "thuytt@ttc-solutions.com.vn",
                txtEmail.text != "huent@ttc-solutions.com.vn",
                txtEmail.text != "hongct@ttc-solutions.com.vn",
                txtEmail.text != "hieupa@ttc-solutions.com.vn",
                txtEmail.text != "khanhnhq@ttc-solutions.com.vn",
                txtEmail.text != "tungbq@ttc-solutions.com.vn",
                txtEmail.text != "dund@ttc-solutions.com.vn",
                txtEmail.text != "hiepnh@ttc-solutions.com.vn",
                txtEmail.text != "sonpn@ttc-solutions.com.vn",
                txtEmail.text != "quyetpv@ttc-solutions.com.vn",
                txtEmail.text != "sinhpv@ttc-solutions.com.vn",
                txtEmail.text != "lampv@ttc-solutions.com.vn",
                txtEmail.text != "baopt@ttc-solutions.com.vn",
                txtEmail.text != "toanlt@ttc-solutions.com.vn",
                txtEmail.text != "datnb@ttc-solutions.com.vn",
                txtEmail.text != "sonnt2@ttc-solutions.com.vn",
                txtEmail.text != "tungtd1@ttc-solutions.com.vn",
                txtEmail.text != "tuannh@ttc-solutions.com.vn",
                txtEmail.text != "dupt@ttc-solutions.com.vn",
                txtEmail.text != "namnt@ttc-solutions.com.vn",
                txtEmail.text != "longtq@ttc-solutions.com.vn",
                txtEmail.text != "sontn@ttc-solutions.com.vn",
                txtEmail.text != "sangtq@ttc-solutions.com.vn",
                txtEmail.text != "namnp2@ttc-solutions.com.vn",
                txtEmail.text != "diennv@ttc-solutions.com.vn",
                txtEmail.text != "cuongnm@ttc-solutions.com.vn",
                txtEmail.text != "cuongnm@ttc-solutions.com.vn",
                txtEmail.text != "ducth@ttc-solutions.com.vn",
                txtEmail.text != "sonbn@ttc-solutions.com.vn"{
            //MARK: tai khoan ko ton tai
            var mycheck4:checkLogin4
            mycheck4 = checkLogin4(nibName: "checkLogin4", bundle: nil)
            self.view.alpha = 1.0
            mycheck4.popup4 = self
            self.presentpopupViewController(popupViewController: mycheck4, animationType: .BottomTop, completion: {()
                -> Void in
            })
            
        }
        else if isValidPassword(passwordId: matkhau) == false {
            //MARK: Email hoac chua chinh xac
            var mycheck2:checkLLogin2
            mycheck2 = checkLLogin2(nibName: "checkLLogin2", bundle: nil)
            self.view.alpha = 1.0
            mycheck2.popup2 = self
            self.presentpopupViewController(popupViewController: mycheck2, animationType: .BottomTop, completion: {()
                -> Void in
            })
            
            txtEmail.text = nil
            txtMatkhau.text = nil
        }
        
    }
    
    
}
func isValidEmail(emailId: String) -> Bool{
    let emailRegEx = "[A-Z0-9a-z._%+-]+@ttc-solutions.com.vn"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailId)
}
func isValidPassword(passwordId:String) -> Bool {
    let passwordRegEx = "(?=.{6,})"
    let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
    return passwordTest.evaluate(with: passwordId)
}


